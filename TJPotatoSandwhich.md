<h3>Trader Joes Potato Patty Sandwhich Recipe</h3>



* Servings: 1
* Prep Time:  5 mins
* Cook Time:  10 mins
* Total Time: 20 mins

Ingredients:

* 1 Trader Joes Vegetable Masala Burger Patty
* 2 Sourdough or any other bread 
* 3 very thin red onion slices 
* 1 tsp of butter
* Finely sliced chillies if available 
* A few thin slices of cheddar cheese 
* Ketchup 

Directions:

1. Cook the frozen patty in a toaster. Geenrally two rounds of highest settings gets it perfectly cooked.
2. Apply the butter on one side of the bread, then cheese and then onion and chillies in the same order to prevent soginess.
3. Roughly chop the patty to fit well in the bread slice
4. Cover with the other slice and cook it in the Sandwhich maker machine to lock the flavors inside . 
5. When the bread is cooked, take it out of the machine and serve with ketchup 
