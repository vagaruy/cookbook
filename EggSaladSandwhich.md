<h3>Egg Salad Sanwdhich Recipe</h3>
<!-- https://www.youtube.com/watch?v=5fZ1GhIrpmY /-->


* Servings: 3
* Prep Time:  20 mins
* Cook Time:  0 mins
* Total Time: 30 mins

Ingredients:

* 6 Large Eggs
* 1 Spring Onion
* 0.5 tsp onion powder
* 3 tpsb Mayonaise
* 1 tsp Rice Vinegar 
* Pepper
* Salt
* White bread 

Directions:

1. Drop the eggs super carefully in a saucepan and cover with water. (Very easy to break the eggs if not careful )
2. Start the heat, let it come to a boil and then simmer for 7-8 mins. 
3. Cool it down under running cold water and test one egg for doneness. Peeling under running water is super easy 
4. Chop the eggs roughly into small portions and put them in a mixing bowl
5. Now finely chop the sprint onion and add to the eggs
6. Add the salt, pepper , vinegar, onion powder and Mayonaise.
7. Mix the whole thing really well till they combine well
8. Get two pieces of bread, add a good amount of the mix and cut it in half and serve ! 
