<h3>Shahe Paneer Recipe</h3>

cookingshooking.com

* Servings: 3
* Prep Time:  10 mins
* Cook Time:  40 mins
* Total Time: 60 mins

Ingredients:

* 2 Medium Tomato
* 1/2 Large Onion
* 12 Cashew
* 5 Almond
* 1 Star Anise
* 2 Cardamom
* 3 Cloves
* 1 Black Cardamom
* Fenugreek Leaves
* Heavy Cream
* Coriander Leaves
* Shah Jeera 
* Turmeric
* Chilli Powder
* Salt to taste

Directions:

1. Chop tomatoes , onion  alongwith cashew almond, anise, cardamom, cloves and cardamom put them in a pot with 1 cup of water and boil till well cooked covered .
2. Fish out the solid spices and then blend them to a smooth paste
3. Sautee some panner with butter, add tumeric and chilli powder and cook. Add saffron if you want 
4. Add the paste and cook it for a while .
5. Add some heavy cream along with salt adn sugar and thicken a bit. 
6. Sprinkle some cilantro and fenugreek dried leaves on top. 