<h3>Simple Dosa Recipe</h3>

Motivated by this 

* Servings: 5
* Prep Time:  36 Hours
* Cook Time:  15 mins

Ingredients:

* 2 Cups Rice (Any rice will do . I used Trader Joes Basmati Rice and turned out pretty good )
* 1/3 Cup Chana Daal 
* 1/2 Cup Urad Dal 
* 1 Tbsp Fenugreek Seeds (methi)
* Salt to taste

Directions:

1. Combine all the dry ingredients and wash thoroughly till the water is running clear. I combine the ingredients in a colander which helps with washing
2. Soak it in plenty of lukewarm water for atleast 5 hours. (I soak it for 12 hours Mix it in the morning and grind in the evening ) 
3. Put everything in vitamix and start pulsing at speed 1 . Adjust the water till the mixture seems to be coming together.
4. Slowly boost it to full speed and grind till the batter is not gritty anymore . It should be almost smooth when rubbed against the fingers 
5. I use the pressure cooker to store it . Mix it well and then on a slightly warm oven store it overnight with the pressure cooker lid on 
6. Next morning, take it out, mix the batter again and warm the over again if cold . Put the batter back (if it is not fermented completeley already)
7. The batter is generally ready by evening. You can see aeration and smell the sourness. If not, let it sit for another 12 hours 
8. Add salt per taste and check the consistency. Add water if too thick 
9. Preheat the aluminum pan at 3 . When hot, lightly coat with oil and spread around.Spray water and repeat oil coating.
10.Add a ladle of batter at the center and move outwards. Wait for it to start to brown slightly and add some oil. 
11.If the pan is well coated, the dosa will pop up without much oil. If not , maybe try thinning the batter slightly for better results. 
