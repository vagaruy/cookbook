<h3> No yeast/rise Naan Recipe </h3>

Motivated by this https://cookingshooking.com/recipe/naan-recipe-tawa-soft-restaurant-style-without-yeast-tandoor-eggs-cookingshooking/

* Servings: 6 Naan Breads 
* Prep Time:  15 mins
* Cook Time:  15 mins

Ingredients:

* 2 1/2 Cup White Flour
* 3/4 Cup Milk 
* 3 Tbsp Oil/Butter
* 1 Tbsp Sugar
* 1 Tsp Baking Powder 
* Salt per taste

Directions:

1. In a large mixing bowl, add milk, oil, sugar, salt and baking powder. Mix everything until the sugar has dissolved.
2. Add the flour, mix everything and start kneading to form soft dough, use milk as required.
3. Let the dough rest for 10 mins . Sprinkle with some flour so it doesn't stick to the bowl.
4. Make 6-8 dough balls from this and start rolling them super thin.(This dictates the crunchiness. The thinner the crunchier)
5. Preheat the oven meanwhile at the highest setting so that the sheet pan gets super hot 
6. Put the oven on high broil to get the charred naan effect. Dump the rolled naan on the pan and let it cook till some brown spots appear on the surface.If the naan is rolled too thick, you may have to cook the other side as well.Suggest keeping it thin.
7. If the dough was sticky, add some flour while rolling. 