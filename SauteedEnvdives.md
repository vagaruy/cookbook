<h3>Sauteed Endives in Balsamic Butter Recipe</h3>
<!-- https://cooking.nytimes.com/recipes/1018295-sauteed-endive-with-balsamic-butter /-->


* Servings: 2
* Prep Time:  10 mins
* Cook Time:  15 mins
* Total Time: 30 mins

Ingredients:

* 3-4 Endvies
* 2 Tbsp Balsamic vinegar
* 3 Tbsp Butter
* Sugar 
* Black pepper
* Oregano leaves 

Directions:

1. Simmer vinegar in a small saucepan for about a minute. Remove from heat and whisk in butter bit by bit. Whisk in sugar and, if desired, some salt and pepper. This will become the sauce for the dish 
2. Heat oil in a searing pan. Add endive halves, cut side down, and sear on medium-high heat for a couple of minutes per side, turning once, until golden brown but still somewhat firm in the middle. I generally cook it till the soft is somewhat starting to cook.  Salt to taste, transfer to a serving dish.
3. Drizzle the dressing on top and dust with some pepper and oregano leaves. serve with more dressing on the side. 
