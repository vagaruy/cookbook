<h3>Shakshuka Recipe</h3>

https://cooking.nytimes.com/recipes/1014721-shakshuka-with-feta

* Servings: 3
* Prep Time:  10 mins
* Cook Time:  50 mins
* Total Time: 70 mins

Ingredients:

* 5 Eggs
* 1 Large Yellow Onion Thinly sliced 
* 1 Large Red BellPepper - Thinly sliced
* 4 Garlic Cloves sliced thinly 
* 1 28Oz can of Tomatoes 
* A bunch of cilantro
* Paprika
* Cumin
* Black Pepper
* Cayenne
* Feta Cheese
* Salt to taste

Directions:

1. Finely slice the Spanish Onions, red pepper and garlic.
2. Heat 2 Tbsp of Olive oil and cook the onions and pepper till they completely soften. Do this in a medium flame to prevent charring. Add salt to expedite this. It's important that the peppers soften very well before proceeding to next step. Can take upto 20 mins. Add more oil if required for cooking. 
3. Add the sliced garlic and cook till the raw aroma goes away.
4. Add the paprika, salt , cayenne and cumin to taste. 
5. Add the can of tomato and cook till the sauce thickens. Taste and adjust the heat and spices. 
6. Once the sauce is fairly set, make some wells in the sauce with a spoon and dump the eggs. Generally 5 works for this recipe.
7. Season the eggs with salt and pepper
8. Add crumbled feta cheese on top but don't mix it in the sauce. 
9. Cook in either a 350C oven till the eggs set / over stovetop with lid closed till set. 
10. Garnish with cilantro if possible.