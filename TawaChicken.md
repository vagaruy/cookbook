<h3>Tawa Chicken Recipe</h3>

* Servings: 2
* Prep Time:  15 mins
* Cook Time:  30 mins
* Total Time: 30 mins

Ingredients:

* 1/2 lb of Boneless Chicken Thighs chopped into bite sized pieces
* 6 gloves of garlic minced
* 1 Tbsp of minced ginger
* 1 Tsp Turmeric
* 1 Tsp Chilli Powder
* 1 Tbsp Oil

Directions:

1. Chop the chicken thigh into bite sized pieces.
2. Mince the garlic and ginger
3. Heat the oil in a flat pan and add some cumin when spluttering
4. Add the ginger and garlic and toast it for a while till the garlic cooks up a bit being careful not to burn the garlic
5. Add the chicekn and let it brown a bit. 
6. Add the turmeric, salt and chilly powder and mix them together.
7. Cover the pan and let the chicken cook.
8. Once cooked, let the water dry off a bit.